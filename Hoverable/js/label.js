var hoverable = 
{
	initialize: function()
	{
		var setPristineState = function(label, field)
		{
			if (field.value) 
			{
				field.classList.remove("js-pristine");
				label.classList.add("js-frozen");
			}
			else
			{
				field.classList.add("js-pristine");	
				label.classList.remove("js-frozen");
			}
		};

		var trySetFieldsIds = function(fields)
		{
			for (var i = 0; i < fields.length; i++) 
			{
				var field = fields[i];
				if (field.id === "") 
				{
					field.id = field.dataset["purpose"].replace(/ /g, '-').toLowerCase();
				}
				field.classList.add("js-hovered-field");


				field.onblur = function(event)
				{
					setPristineState(this.nextSibling, this);
				};
			}
		};

		var createRoots = function(fields)
		{
			var createElement = function(tagName, className)
			{
				var element = document.createElement(tagName);
				element.className = className;
				return element;
			}; 

			for (var i = 0; i < fields.length; i++) 
			{
				var field = fields[i];
				var container = createElement("div", "js-hovered-field-container");
				var label = createElement("label", "js-hovering-label");
				

				label.innerHTML = field.dataset["purpose"];
				label.htmlFor = field.id;

				field.parentElement.appendChild(container);
				container.appendChild(field);
				container.appendChild(label);

				setPristineState(label, field);
			}
		};

		var fields = document.querySelectorAll(".js-label");
		trySetFieldsIds(fields);		
		createRoots(fields);
	}	
};

hoverable.initialize();


