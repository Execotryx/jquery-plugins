$.fn.label = function(options)
{
	var getSettings = function()
	{
		return $.extend({width: "auto", background: "#fff", height: "auto"}, options);
	}

	var trySetFieldId = function(field)
	{
		return function()
		{
			if (field.prop("id") === "") 
			{
				var id = field.data("purpose").replace(/ /g, '-').toLowerCase();
				field.prop("id", id);
			}
		}();
	}

	var trySetInitialState = function(field, fieldRoot)
	{
		return function()
		{
			field.addClass("js-pristine");	
			fieldRoot.find(".js-hovering-label").removeClass("js-frozen");		
		}();
	}

	this.each(function()
	{
		var settings = getSettings();
		var field = $(this);	
		
		trySetFieldId(field);

		var fieldRoot = $("<div></div>")
									.html($("<label></label>")
											.addClass("js-hovering-label")
											.text(field.data("purpose"))
											.prop("for", field.prop("id")))
									.addClass("js-hovered-field-container")
									.prop("id", field.prop("id")+"-container")
									.css(settings);
		field.parent().append(fieldRoot);
		field.detach().prependTo(fieldRoot)
						.removeClass("js-label")
						.addClass("js-hovered-field");

		trySetInitialState(field, fieldRoot);

		field.on("blur.label", function(event)
		{
			if (field.val() !== "") 
			{
				field.removeClass("js-pristine");
				fieldRoot.find(".js-hovering-label").addClass("js-frozen");
			}
			else
			{
				trySetInitialState(field, fieldRoot);
			}
		});
	});
};

$(document).ready(function()
{
	$(".js-label").label();
});