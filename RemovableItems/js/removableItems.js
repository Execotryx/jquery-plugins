$.fn.removableItems = function() {
	var listRemovableItems = this;

	if (!listRemovableItems.is("ol") && !listRemovableItems.is("ul")) {
		alert("This plugin can be used only with lists (ordered and unordered).");
		return;
	}

	var itemNumber = 1;
	var listRoot = listRemovableItems.parent();
	$("<button></button>").text("Add Element")
						  .addClass("removable add enabled")
						  .appendTo(listRoot);

	$("<button></button>").text("Remove Element")
						  .addClass("removable remove")
						  .appendTo(listRoot);

	if (listRemovableItems.children().length > 0)
	{
		$(".removable.remove").addClass("enabled");
	}

	listRoot.on("click.removable", ".remove.enabled",
  		function(event) {
  		listRemovableItems.children().last().remove();
  		if (listRemovableItems.children().length === 0)
  			{
  				$(this).removeClass("enabled")
  				alert("No items to remove.")
  			}
  		}).on("click.removable", ".add.enabled", function(event) {
  		if (listRemovableItems.children().length === 0)
	  		{
	  			$(".removable.remove").addClass("enabled");
	  		}
  		$("<li></li>").text("Item " + (itemNumber++))
  					  .appendTo(listRemovableItems);
  		console.log("item added");
  		});
};
