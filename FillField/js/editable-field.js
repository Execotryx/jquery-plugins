$.fn.editableField = function()
{
  var placeholder = this;

  placeholder.addClass("editable-field-placeholder");
  var editField = $("<input type='text'></input>").val(placeholder.text()).addClass("editable-field");
  placeholder.on("dblclick.editable", function(event)
  {
    editField.on("blur", function(event)
    {
      editField.removeClass("in-edit");
      placeholder.text(editField.val());
      placeholder.trigger("editEnd.editable");
    });

    placeholder.append(editField.show())
               .trigger("editStart.editable");
  }).on("editStart.editable", function(event)
  {
    editField.addClass("in-edit").focus();
  }).on("editEnd.editable", function(event)
  {
    editField.remove();
  });
  
};
$(document).ready(function()
{
  $.each($(".js-editable-field"), function(index, field) {$(field).editableField();});
});
